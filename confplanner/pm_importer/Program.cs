﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using backend;
using backend.confPlanner;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;

namespace pm_importer
{
    class Program
    {
        static void Main(string[] args)
        {
            //delete all existing private meetings
            foreach (string file in Directory.GetFiles("Topics"))
            {
                if (File.ReadAllText(file).Contains("<forPrivateMeeting>true</forPrivateMeeting>") == true)
                {
                    File.Delete(file);
                    Console.WriteLine("deleted {0}", file);
                }
            }

            PrivateMeetingDictionary result = new PrivateMeetingDictionary();

            GlobalRules rules = ConferencePlanner.loadRules();

            ConferencePlanner.loadSpeakers();
            List<string> existingSpeakerIDs = Speaker.speakers.Keys.ToList();


            int sessionMinutes =
                rules.sessionTiming.sessionDuration.Minutes + rules.breakSchedule.brakeTime.Minutes;

            //GlobalRules rules = ConferencePlanner.load

            using (StreamReader reader = new StreamReader("privateMeetings.csv"))
            {
                //read and ignore first (header) line
                string line = reader.ReadLine();

                while ((line = reader.ReadLine()) != null)
                {
                    string[] data = line.Split(';');

                    if (data.Length < 7)
                    {
                        data = line.Split(',');
                    }

                    if (data.Length < 7)
                    {
                        data = line.Split('\t');
                    }

                    string pmID = data[6];
                    string pmName = data[5];
                    string attendee = data[4];

                    int minutes;
                    if (int.TryParse(data[1], out minutes) == false)
                    {
                        continue;
                    }

                    if (result.ContainsKey(pmID) == false)
                    {
                        Topic temp = new Topic(pmID, 
                            speakers: new List<string>(),
                            speakersSecondary: new List<string>(),                           
                            forPrivateMeeting: true, description: pmName,
                            attendees: new AttendeesScheduled.SessionOnly(minutes / sessionMinutes));


                        result.Add(pmID, temp);
                    }

                    if (Speaker.speakers.ContainsKey(attendee) == false)
                    {
                        XmlWriterSettings settings = new XmlWriterSettings { Indent = true };

                        using (XmlWriter w = 
                            XmlWriter.Create(String.Format("Speakers/{0}.xml", attendee), settings))
                        {
                            DataContractSerializer ser = new DataContractSerializer(typeof(Speaker));
                            ser.WriteObject(w, new Speaker(attendee));
                        }                        
                    }


                    if (result[pmID].primarySpeakerIDs.Contains(attendee) == false)
                    {
                        result[pmID].primarySpeakerIDs.Add(attendee);
                    }
                }

                int i = 901;
                foreach (Topic topic in result.Values.ToList())
                {
                    XmlWriterSettings settings = new XmlWriterSettings { Indent = true };

                    string file = string.Format("{0} PM - {1}", i++, topic.description);

                    //replace forbidden chars
                    file = file.Replace(@"/", "+");
                    file = file.Replace(@"#", "+");
                    file = file.Replace(@"\", "+");
                    file = file.Replace(@"<", "+");
                    file = file.Replace(@">", "+");
                    file = file.Replace(@"?", "+");
                    file = file.Replace(@"*", "+");
                    file = file.Replace(@"|", "+");

                    using (XmlWriter w =
                        XmlWriter.Create(String.Format("Topics/{0}.xml", file), settings))
                    {
                        DataContractSerializer ser = new DataContractSerializer(typeof(Topic));
                        ser.WriteObject(w, topic);
                    }
                }             
            }

            Console.WriteLine("done...");

            if (args != null)
            {
                if (args.Contains("--exit") == false)
                {
                    Console.WriteLine("type exit to leave");
                    while (Console.ReadLine() != "exit") ;
                }
            }
        }
    }

    public class PrivateMeetingDictionary : Dictionary<string, Topic>
    {
    }
}
