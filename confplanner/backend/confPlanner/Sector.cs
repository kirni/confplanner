﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace backend.confPlanner
{
    /// <summary>
    /// a speaker is somebody who can hold a session of a topic; speakers are referenced in topics
    /// </summary>
    [DataContract]
    public class Sector
    {
        /// <summary>
        /// must be unique over all speakers; generated in constructor
        /// </summary>
        [DataMember]
        public string id { get; private set; }
        [DataMember]
        public string name { get; private set; }
        [DataMember]
        public int seats { get; private set; }
        [DataMember]
        public Availability availability { get; private set; }

        [DataMember]
        public bool forPrivateMeeting { get; private set; }

        public int remainingSessions { get; set; }
        public int possibleSessions { get; set; }

        public SortedDictionary<double, Topic> linkedTopics { get; private set; } = new SortedDictionary<double, Topic>();

        /// <summary>
        /// dictionary containing all currently known speakers
        /// </summary>
        public static Dictionary<string, Sector> sectors = new Dictionary<string, Sector>();

        /// <summary>
        /// created unique speaker id based on first and last name
        /// </summary>
        /// <returns>unique speaker id</returns>
        private string generateUniqueID()
        {
            string tempID = name;

            while (sectors.ContainsKey(tempID) == true)
            {
                tempID += "d";
            }

            return tempID;
        }

        /// <summary>
        /// links a topic to the sector if the secotr has still sessions left, 
        /// or the weigt of the topic is smaller than a already linked topic
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="topic"></param>
        /// <returns></returns>
        public bool linkTopic(double weight, Topic topic)
        {
            if (linkedTopics == null)
            {
                linkedTopics = new SortedDictionary<double, Topic>();
            }

            if (remainingSessions - topic.requiredSessions >= 0)
            {
                linkedTopics.Add(weight, topic);
                remainingSessions -= topic.requiredSessions;
                topic.linkedToSector = true;
                topic.assignedSector = this;

                Console.WriteLine("linked Topic: {0} to Sector: {1}", topic, this);
                Console.WriteLine("remaining sessions in sector: {0}", remainingSessions);
                Console.WriteLine();

                topic.weight = weight;

                return true;
            }
            else
            {
                foreach (double key in linkedTopics.Keys)
                {
                    //already linked topic found which has an higher weight
                    if (weight < key)
                    {
                        //check if the linked topic really can be replaced
                        if (linkedTopics[key].requiredSessions >= topic.requiredSessions)
                        {
                            //unlink linked topic
                            Topic temp = linkedTopics[key];
                            remainingSessions += temp.requiredSessions;
                            temp.linkedToSector = false;
                            temp.assignedSector = null;
                            linkedTopics.Remove(key);

                            Console.WriteLine("unlinked Topic: {0} from Sector: {1}", temp, this);

                            //link new topic
                            remainingSessions -= topic.requiredSessions;
                            topic.linkedToSector = true;
                            topic.assignedSector = this;
                            linkedTopics.Add(weight, topic);

                            Console.WriteLine("linked Topic: {0} to Sector: {1}", topic, this);
                            Console.WriteLine("remaining sessions in sector: {0}", remainingSessions);
                            Console.WriteLine();

                            topic.weight = weight;                           

                            return true;

                        }                     
                    }
                }
            }

            return false;
        }

        public Sector(string name, Availability availability = null, bool ignore = false)
        {
            this.name = name;
            this.availability = availability;
            this.id = generateUniqueID();

            if (ignore == false)
            {
                sectors.Add(this.id, this);
            }
        }

        /// <summary>
        /// adds deserialized speakers to static speakers list
        /// </summary>
        /// <param name="context"></param>
        [OnDeserialized()]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            try
            {
                sectors.Add(this.id, this);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("ERROR: sector key {0} is already existing", this.id);
                throw e;
            }
        }

        public bool canHostTopic(Topic topic, bool ignoreSeats = false)
        {
            //private meetings are always possible in sectors
            if (ignoreSeats == true)
            {
                return topic.forPrivateMeeting == this.forPrivateMeeting;
            }

            return topic.attendeesPerSession <= this.seats && topic.forPrivateMeeting == this.forPrivateMeeting;
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", id, name);
        }

        public static Sector getSample()
        {
            return new Sector("room_salzburg", Availability.getSample(), true)
            {
                forPrivateMeeting = false,
            };
        }
    }
}

