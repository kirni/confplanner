﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace backend.confPlanner
{
    [DataContract]
    public class GlobalRules
    {
        /// <summary>
        /// gloabe rules for breake times
        /// </summary>
        [DataMember]
        public Break breakSchedule { get; private set; }

        [DataMember]
        public MatchingRules matchingRules { get; private set; }

        /// <summary>
        /// global rules for all speakers
        /// </summary>
        [DataMember]
        SpeakerRules speakerRules;

        /// <summary>
        /// defines how the timing for sessions is organized
        /// </summary>
        [DataMember]
        public SessionTiming sessionTiming { get; private set; }       

        public static GlobalRules getSample()
        {
            return new GlobalRules()
            {
                 breakSchedule = Break.getSample(),
                 speakerRules = SpeakerRules.getSample(),
                 sessionTiming = SessionTiming.getSample(),     
                 matchingRules = MatchingRules.getSample(),
            };
        }

        /// <summary>
        /// defines how is the session timing organized globally
        /// </summary>
        [DataContract]
        public class SessionTiming
        {
            /// <summary>
            /// timing for each day
            /// </summary>
            [DataMember]
            public List<DateRange> days { get; private set; }

            [DataMember]
            public TimeSpan sessionDuration { get; private set; }

            public static SessionTiming getSample()
            {
                return new SessionTiming()
                {
                    days = new List<DateRange>((new DateRange[]
                    {
                        new DateRange(new DateTime(2019,10,10,8,30,0), new DateTime(2019,10,10,18,30,0)),
                        new DateRange(new DateTime(2019,10,11,8,30,0), new DateTime(2019,10,11,18,30,0)),
                        new DateRange(new DateTime(2019,10,12,8,30,0), new DateTime(2019,10,12,18,30,0)),
                    })),
                    sessionDuration = new TimeSpan(0,50,0),
                };
            }                  
          
        }

        [DataContract]
        public class MatchingRules
        {
            [DataMember]
            public bool restrictTopicsToOneSector { get; private set; }

            public static MatchingRules getSample()
            {
                return new MatchingRules()
                {
                    restrictTopicsToOneSector = false,
                };
            }
        }
       
        /// <summary>
        /// global rules for speakers
        /// </summary>
        [DataContract]
        public class SpeakerRules
        {
            /// <summary>
            /// max sessions in a row for speakers
            /// </summary>
            [DataMember]
            int maxStackedSessions;

            public static SpeakerRules getSample()
            {
                return new SpeakerRules()
                {
                    maxStackedSessions = 2,
                };
            }
        }

        [DataContract]
        public class Break
        {
            [DataMember]
            public TimeSpan brakeTime { get; private set; }

            [DataMember]
            public List<DateRange> speakerFreeSlots { get; private set; }

            public bool intersectsBrake(TimeSlot slot)
            {
                return intersectsBrake(slot.range);
            }

            public bool intersectsBrake(DateRange range)
            {
                foreach (DateRange inst in speakerFreeSlots)
                {
                    if (inst.isIntersecting(range) == true)
                    {
                        return true;
                    }
                }

                return false;
            }

            public static Break getSample()
            {
                return new Break()
                {
                    brakeTime = new TimeSpan(0, 10, 0),
                    speakerFreeSlots = new List<DateRange>((new DateRange[]
                {
                        new DateRange(new DateTime(2019,10,10,11,30,0), new DateTime(2019,10,10,14,30,0)),
                        new DateRange(new DateTime(2019,10,11,11,30,0), new DateTime(2019,10,11,14,30,0)),
                        new DateRange(new DateTime(2019,10,12,11,30,0), new DateTime(2019,10,12,14,30,0)),
                })),
                };
            }   
        }
    }
}
