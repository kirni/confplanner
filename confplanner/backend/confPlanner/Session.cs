﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace backend.confPlanner
{   

    /// <summary>
    /// a Session is somebody who can hold a session of a topic; Sessions are referenced in topics
    /// </summary>
    [DataContract]
    class Session
    {
        /// <summary>
        /// must be unique over all Sessions; generated in constructor
        /// </summary>
        [DataMember]
        string id;
        /// <summary>
        /// name of the session, does not need to be unique but is used as base for unique id;
        /// </summary>
        [DataMember]
        string name;
        /// <summary>
        /// id of the topic of the session; used to reference the topic object
        /// </summary>
        [DataMember]
        string topicID;
        /// <summary>
        /// id of the assigned sector to the session; used to referecne the secotr object
        /// </summary>
        [DataMember]
        string sectorID;
        /// <summary>
        /// list of speaker IDs; used to reference the speaker IDs
        /// </summary>
        [DataMember]
        List<string> speakerIDs;

        [DataMember]
        DateTime startTime;
        [DataMember]
        DateTime endTime;

        private static Random random = new Random();
        public static string RandomString(int length = 5)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// dictionary containing all currently known Sessions
        /// </summary>
        public static Dictionary<string, Session> sessions = new Dictionary<string, Session>();

        public static List<Session> instantiateSessions(GlobalRules rules, List<Topic> topics)
        {
            List<Session> tempSessions = new List<Session>();

            foreach (Topic topic in topics)
            {
                int requiredSessions = topic.requiredSessions;

                for (int i = 0; i < requiredSessions; i++)
                {
                    Session temp = new Session()
                    {
                        name = topic.id,
                        topicID = topic.id,
                    };

                    temp.generateUniqueID();

                    tempSessions.Add(temp);
                }                
            }

            return tempSessions;
        }

        /// <summary>
        /// created unique Session id based on first and last name
        /// </summary>
        /// <returns>unique Session id</returns>
        private void  generateUniqueID()
        {
            string tempID;

            do
            {
                tempID = RandomString();
            } while (sessions.ContainsKey(tempID) == true);           

            this.id = tempID;

            sessions.Add(this.id, this);
        }
      
        /// <summary>
        /// adds deserialized Sessions to static Sessions list
        /// </summary>
        /// <param name="context"></param>
        [OnDeserialized()]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            sessions.Add(this.id, this);
        }      
    }
}
