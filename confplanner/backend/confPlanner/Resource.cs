﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace backend.confPlanner
{
    /// <summary>
    /// a Resource is somebody who can hold a session of a topic; Resources are referenced in topics
    /// </summary>
    [DataContract]
    class Resource
    {
        /// <summary>
        /// must be unique over all Resources; generated in constructor
        /// </summary>
        [DataMember]
        string id;
        [DataMember]
        string type;
        [DataMember]
        string name;
        [DataMember]
        Availability availability;

        /// <summary>
        /// dictionary containing all currently known Resources
        /// </summary>
        public static Resources currentResources = new Resources();

        /// <summary>
        /// created unique Resource id based on first and last name
        /// </summary>
        /// <returns>unique Resource id</returns>
        private string generateUniqueID(string name, string type)
        {
            string tempID = name + "_" + type;

            //add counter if resource of same type and name is already existing
            if (currentResources.ContainsKey(type) == true)
            {
                tempID += currentResources[type].Count;
            }

            return tempID;
        }

        public Resource(string name, string type, Availability availability = null, bool ignore = false)
        {
            this.availability = availability;
            this.type = type;
            this.name = name;
            this.id = generateUniqueID(name, type);

            if (ignore == false)
            {
                currentResources.Add(this);
            }
        }

        /// <summary>
        /// adds deserialized Resources to static Resources list
        /// </summary>
        /// <param name="context"></param>
        [OnDeserialized()]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            currentResources.Add(this);
        }

        public static Resource getSample()
        {
            return new Resource("projector", "BrandAndCounter", Availability.getSample(), true);
        }

        public class Resources : Dictionary<string, List<Resource>>
        {
            public void Add(Resource resource)
            {
                //create new list if no resource with type
                if (this.ContainsKey(resource.type) == false)
                {
                    this.Add(resource.type, new List<Resource>());
                }

                this[resource.type].Add(resource);
            }
        }
    }
}
