﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;

namespace backend.confPlanner
{
    /// <summary>
    /// a speaker is somebody who can hold a session of a topic; speakers are referenced in topics
    /// </summary>
    [DataContract]
    public class Speaker
    {
        /// <summary>
        /// must be unique over all speakers; generated in constructor
        /// </summary>
        [DataMember]
        public string id { get; private set; }     
        [DataMember]
        Availability availability;

        [DataMember]
        public bool noBrakeRequired { get; private set; }

        public List<TimeSlot> assignedSlots = new List<TimeSlot>();

        /// <summary>
        /// dictionary containing all currently known speakers
        /// </summary>
        public static Dictionary<string, Speaker> speakers = new Dictionary<string, Speaker>();
      
        public Speaker(string id, Availability availability = null, bool ignore = false)
        {
            this.id = id;
            this.availability = availability;

            if (ignore == false)
            {
                if (this.id == null)
                {
                    Console.WriteLine("speaker id is null");
                    return;
                }
                if (speakers.ContainsKey(this.id) == true)
                {
                    Console.WriteLine("speaker {0} already existing", this);
                    return;
                }

                speakers.Add(this.id, this);
            }
        }

        public bool hasValidBrake(TimeSlot slot)
        {

            //must have brake of nothing assigned yet
            if (assignedSlots == null)
            {
                return true;
            }

            if (noBrakeRequired == true)
            {
                return true;
            }

            //timeslot has no issue if it is not intersecting any brakes
            if (ConferencePlanner.currentRules.breakSchedule.intersectsBrake(slot) == false)
            {
                return true;
            }

            List<int> possibleBrakes = ConferencePlanner.brakeTracks[slot.range.Start.Day];

            //at least two potential brake slots have to remain per day to make assignment possible 
            int remainingBrakes = possibleBrakes.Count;

            foreach (int track in possibleBrakes)
            {           
                foreach (TimeSlot assignedSlot in assignedSlots)
                {                    
                    //speaker has already a slot which intersectes brake on a day
                    if (track == assignedSlot.track)
                    {                      
                        remainingBrakes--;
                    }

                    //not enough remaining brake slots for further assignemnt
                    if (remainingBrakes < 2)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool slotValid(TimeSlot slot, Stream message = null)
        {
            Stream temp = message;
            if (temp == null)
            {
                temp = Console.OpenStandardError();
                //temp = Debug;
            }
            StreamWriter writer = new StreamWriter(temp);
            writer.WriteLine("\t\ttry to match {0} to session {1} in {2}",
                this.id, slot.track, slot.sectorRef);

            if (assignedSlots == null)
            {
                assignedSlots = new List<TimeSlot>();
            }

            foreach (TimeSlot inst in this.assignedSlots)
            {
                if (inst.intersectsOther(slot) == true)
                {
                    writer.WriteLine("\t\t\tspeaker already assigned to {0}", inst.topicRef);
                    writer.Flush();

                    return false;
                }
            }

            if (availability != null)
            {
                if (availability.isAvailable(slot.range) == false)
                {
                    writer.WriteLine("t\t\t\tspeaker not available due to blacklist", this, slot);
                    writer.Flush();
                    return false;
                }
            }

            if (hasValidBrake(slot) == false)
            {
                writer.WriteLine("\t\t\tviolates brake times", this, slot);
                writer.Flush();
                return false;
            }

            return true;

        }

        public void assignSlot(TimeSlot slot)
        {
            if (assignedSlots == null)
            {
                assignedSlots = new List<TimeSlot>();
            }

            if (slotValid(slot))
            {
                assignedSlots.Add(slot);
            }
        }

        public bool containsID(string toCheck)
        {
            string check = toCheck.ToLower();
            string lowerID = this.id.ToLower();

            if (check.Contains(lowerID) == true)
            {
                return true;
            }

            string[] parts = lowerID.Split(' ');

            if (parts.Count() < 2)
            {
                //Console.WriteLine("no proper first and last name for {0} found", this.id);
                return false;
            }

            string first = parts[0];
            string last = parts[1];

            if (check.Contains(first) && check.Contains(last))
            {
                return true;
            }

            first = first.Replace("ä", "ae");
            first = first.Replace("ö", "oe");
            first = first.Replace("ü", "ue");
            first = first.Replace("ß", "ss");

            last = last.Replace("ä", "ae");
            last = last.Replace("ö", "oe");
            last = last.Replace("ü", "ue");
            last = last.Replace("ß", "ss");

            if (check.Contains(first) && check.Contains(last))
            {
                return true;
            }

            return false;
        }

        public void removeSlot(TimeSlot slot)
        {
            Console.WriteLine("remove slot: {0} from speaker: {1}", slot, this);
            //assignedSlots.Remove(slot);
        }

        public override string ToString()
        {
            return id;
        }

        /// <summary>
        /// adds deserialized speakers to static speakers list
        /// </summary>
        /// <param name="context"></param>
        [OnDeserialized()]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            try
            {
                speakers.Add(this.id, this);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("ERROR: topic key {0} is already existing", this.id);
                throw e;
            }
        }

        public static Speaker getSample()
        {
            return new Speaker("Max Mustermann", Availability.getSample(), true)
            {
                noBrakeRequired = false,
            };
        }
    }
}
