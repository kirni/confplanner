﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;

namespace backend.confPlanner
{
    /// <summary>
    /// helper class to define availability of speakers, resources, etc..
    /// </summary>
    [DataContract]
    public class Availability
    {      
        /// <summary>
        /// defines a list of not avaiable timeslots with a start and end date each
        /// </summary>
        [DataMember]
        List<DateRange> blacklist;

        public double blacklistHours()
        {
            if (blacklist == null)
            {
                return 0;
            }

            double hours = 0;

            foreach (DateRange range in blacklist)
            {
                hours += (range.End - range.Start).TotalHours;
            }

            return hours;
        }


        public bool isAvailable(DateRange range, Stream message = null)
        {
            foreach (DateRange except in blacklist)
            {
                if (except.isIntersecting(range) == true)
                {                   
                    return false;
                }
            }

            return true;
        }

        public List<DateRange> selectAvailables(List<DateRange> src)
        {
            List<DateRange> result = new List<DateRange>();

            foreach (DateRange inst in src)
            {
                if (this.isAvailable(inst) == true)
                {
                    result.Add(inst);
                }
            }

            return result;
        }

        public static Availability getSample()
        {
            return new Availability()
            {               
                blacklist = new List<DateRange>(new DateRange[]
                    {
                        new DateRange(
                            new DateTime(2019,10,13,10,10,10,10),
                            new DateTime(2019, 11, 7, 10, 10, 10, 10)),
                    }),
            };
        }
    }

    public interface IRange<T>
    {
        T Start { get; }
        T End { get; }
        bool Includes(T value);
        bool Includes(IRange<T> range);
    }

    [DataContract]
    public class DateRange : IRange<DateTime>
    {
        public DateRange(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }

        public bool isIntersecting(DateRange other)
        {
            if (other.Start >= this.Start && other.Start < this.End)
            {
                return true;
            }

            if (other.End > this.Start && other.End <= this.End)
            {
                return true;
            }

            if (other.End == this.End && other.Start == this.Start)
            {
                return true;
            }

            /*if (this.Start >= other.Start && this.Start <= other.End)
            {
                return true;
            }

            if (this.End >= other.Start && this.End <= other.End)
            {
                return true;
            }*/

            return false;
        }

        public override string ToString()
        {
            //WABU request
            //return string.Format("Start: {0} End: {1}", Start, End);
            return string.Format("{0}", Start.ToString("dd HH:mm"));
        }

        public override int GetHashCode()
        {
            return Start.ToString().GetHashCode() ^ End.ToString().GetHashCode();
        }

        [DataMember]
        public DateTime Start { get; private set; }
        [DataMember]
        public DateTime End { get; private set; }

        public bool Includes(DateTime value)
        {
            return (Start <= value) && (value <= End);
        }

        public bool Includes(IRange<DateTime> range)
        {
            return (Start <= range.Start) && (range.End <= End);
        }
    }
}
