﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;

namespace backend.confPlanner
{

    public class TimeSlot
    {
        /// <summary>
        /// assigned sector
        /// </summary>
        public Sector sectorRef;

        public int track;

        public DateRange range;

        //assigned topic
        public Topic topicRef;

        public bool isForced;

        public TimeSlot next;

        private int cAppSpeakers { get { return 25; } }

        public string getAppCSVentry(char separator = ',')
        {
            string result = "";

            //Unique ID
            result += this.GetHashCode().ToString();
            result += separator;

            //Name
            result += topicRef.description;
            result += separator;

            //description
            //TODO: description placeholder
            result += "";
            result += separator;

            //activity code
            result += "";
            result += separator;

            //track
            result += "";
            result += separator;

            //tags
            string temp = "";
            topicRef.tags.ForEach(x => temp += (x + ","));
            temp.Remove(temp.Length - 1);
            result += "\"" + temp  + "\"";
            result += separator;

            //start time
            result += range.Start.ToString("MM/dd/yyyy HH:mm tt", CultureInfo.InvariantCulture);
            result += separator;

            //end time
            result += range.End.ToString("MM/dd/yyyy HH:mm tt", CultureInfo.InvariantCulture);
            result += separator;

            //location name
            result += sectorRef.name;
            result += separator;

            //parent activity unique ID
            result += "";
            result += separator;

            //group list
            result += "";
            result += separator;

            //live Q&A enabled
            result += "FALSE";
            result += separator;

            List<string> speakers = new List<string>();

            if (topicRef.primarySpeakerIDs != null)
            {
                speakers.AddRange(topicRef.primarySpeakerIDs);
            }
            if (topicRef.secondarySpeakerIDs != null)
            {
                speakers.AddRange(topicRef.secondarySpeakerIDs);
            }
            if (topicRef.optionalSpeakerIDs != null)
            {
                speakers.AddRange(topicRef.optionalSpeakerIDs);
            }

            //speaker
            for (int i = 0; i < cAppSpeakers; i++)
            {
                string speaker = "";
                if (speakers.Count > 0)
                {
                    speaker = speakers.First();
                    speakers.Remove(speaker);
                }

                //Speaker 1 Display Name,",
                result += speaker;
                result += separator;
                //Speaker 1 First Name,",
                result += "";
                result += separator;
                //Speaker 1 Last Name,",
                result += "";
                result += separator;
                //Speaker 1 Role,",
                result += "";
                result += separator;
                //Speaker 1 Title,",
                result += "";
                result += separator;
                //Speaker 1 Bio,",
                result += "";
                result += separator;
                //Speaker 1 Email One,",
                result += "";
                result += separator;
                //Speaker 1 Email Two,",
                result += "";
                result += separator;
                //Speaker 1 Organization Name,",
                result += "";
                result += separator;
            }
            //Delete
            result += "";
            //result += separator;

            return result;
        }

        public override string ToString()
        {
            string result = string.Format("{0,10}: {1} Sesion: {2} Sec: {3} - ",
                topicRef,
                range,
                track,
                sectorRef);

            if (topicRef != null)
            {
                foreach (string inst in topicRef.primarySpeakerIDs)
                {
                    result += inst;
                    result += ", ";
                }
            }

            return result;
        }

        public override int GetHashCode()
        {
            if (topicRef == null)
            {
                return range.GetHashCode();
            }
            return range.GetHashCode() ^ topicRef.GetHashCode();
        }

        public bool intersectsOther(TimeSlot other)
        {
            return this.range.isIntersecting(other.range);
        }
    }

    class TimeSlotCollection : Collection<TimeSlot>
    {
        public Dictionary<Topic, SortedDictionary<double, Sector>> weightSectors(List<Topic> topics)
        {
            List<double> weights = new List<double>();
            Dictionary<Topic, SortedDictionary<double, Sector>> result = new Dictionary<Topic, SortedDictionary<double, Sector>>();

            //go trough all topics
            foreach (Topic topic in topics)
            {
                result.Add(topic, new SortedDictionary<double, Sector>());

                TimeSlotCollection supportsTopic = this[topic];

                List<Sector> possibleSectors = supportsTopic.getSectores();

                //go trough all sectores that can host the current topic
                foreach (Sector sector in possibleSectors)
                {
                    double weight = 0;

                    if (sector.forPrivateMeeting == true)
                    {
                        weight = topic.totalAttendees - topic.requiredSessions * sector.seats + 50000;
                    }
                    else
                    {

                        double blackListHours = 1;

                        if (topic.availability != null)
                        {
                            blackListHours = topic.availability.blacklistHours();
                        }

                        double wasteFactor = (double)sector.seats / (double)topic.attendeesPerSession;
                        wasteFactor = Math.Pow(wasteFactor, 5);
                        double attendeeSum = supportsTopic[sector].possibleAttendees() - topic.totalAttendees;

                        weight = attendeeSum * wasteFactor / blackListHours;
                    }

                    //hacky solution if coincidently two sectors have the same weight;
                    while (weights.Contains(weight) == true)
                    {
                        weight++;
                    }

                    weights.Add(weight);

                    result[topic].Add(weight, sector);
                }
            }

            return result;
        }

        public void addRange(TimeSlot[] data)
        {
            foreach (TimeSlot slot in data)
            {
                this.Add(slot);
            }
        }

        public void exportPrivateMeetingCSV(string dest, string idMapping, string separator = ";")
        {
            Dictionary<string, string> speakerIDs = new Dictionary<string, string>();

            string[] mappingLines = File.ReadAllLines(idMapping);

            foreach (Speaker speaker in Speaker.speakers.Values.ToList())
            {
                string line = mappingLines.FirstOrDefault(stringToCheck => speaker.containsID(stringToCheck));

                if (line != null && line != "")
                {
                    string id = line.Split(',')[0];
                    id = id.Replace("\"", "");
                    speakerIDs.Add(speaker.id, id);
                }
            }

            using (StreamWriter writer = new StreamWriter(dest))
            {
                writer.WriteLine("Unique ID{0}Attendee Unique IDs{0}Appointment Name{0}Description{0}Start Time{0}End Time{0}Location Name",
                    separator);
                List<string> ignored = new List<string>();

                foreach (TimeSlot slot in this.getPrivateMeetings())
                {
                    int maxAttendee = 9;
                    int requiredEntries = 1 + slot.topicRef.primarySpeakerIDs.Count / maxAttendee;
                    List<string> done = new List<string>();

                    for (int i = 1; i <= requiredEntries; i++)
                    {
                        string count = "";

                        if (requiredEntries > 1)
                        {
                            count = i.ToString();
                        }

                        if (done.Count == slot.topicRef.primarySpeakerIDs.Count)
                        {
                            continue;
                        }

                        writer.Write("{0}{1}{2}", slot.topicRef.id, count, separator);

                        string speakers = "";

                        int assigned = 0;
                        foreach (string speaker in slot.topicRef.primarySpeakerIDs)
                        {
                            if (ignored.Contains(speaker))
                            {
                                continue;
                            }
                            if (done.Contains(speaker))
                            {
                                continue;
                            }
                            if (speakerIDs.ContainsKey(speaker) == false)
                            {
                                Console.WriteLine("WARNING: could not find {0} in {1}", speaker, idMapping);
                                ignored.Add(speaker);
                                continue;
                            }

                            done.Add(speaker);

                            speakers += speakerIDs[speaker];
                            speakers += ",";

                            if (++assigned >= maxAttendee)
                            {
                                break;
                            }
                        }

                        writer.Write("{0}{1}", speakers, separator);

                        writer.Write("{0}{1}", slot.topicRef.id, separator);

                        string multipleSessionsText = "";

                        if (requiredEntries > 1)
                        {
                            multipleSessionsText = string.Format(" Invitation {0}/{1}", i, requiredEntries);
                        }

                        writer.Write("{0}{1}{2}", slot.topicRef.description, multipleSessionsText, separator);

                        writer.Write("{0}{1}", slot.range.Start, separator);
                        writer.Write("{0}{1}", slot.range.End, separator);
                        writer.Write("{0}{1}", slot.sectorRef.id, writer.NewLine);
                    }
                }
            }
        }

        static List<string> AppCSVHeadings = new List<string>(new string[] 
        {
            "Unique ID,",
            "Name,",
            "Description,",
            "Activity Code,",
            "Track,",
            "Tags (comma-separated),",
            "Start Time,",
            "End Time,",
            "Location Name,",
            "Parent Activity Unique ID,",
            "Group List (comma-separated),",
            "Live Q&A Enabled,",
            "Speaker 1 Display Name,",
            "Speaker 1 First Name,",
            "Speaker 1 Last Name,",
            "Speaker 1 Role,",
            "Speaker 1 Title,",
            "Speaker 1 Bio,",
            "Speaker 1 Email One,",
            "Speaker 1 Email Two,",
            "Speaker 1 Organization Name,",
            "Speaker 2 Display Name,",
            "Speaker 2 First Name,",
            "Speaker 2 Last Name,",
            "Speaker 2 Role,",
            "Speaker 2 Title,",
            "Speaker 2 Bio,",
            "Speaker 2 Email One,",
            "Speaker 2 Email Two,",
            "Speaker 2 Organization Name,",
            "Speaker 3 Display Name,",
            "Speaker 3 First Name,",
            "Speaker 3 Last Name,",
            "Speaker 3 Role,",
            "Speaker 3 Title,",
            "Speaker 3 Bio,",
            "Speaker 3 Email One,",
            "Speaker 3 Email Two,",
            "Speaker 3 Organization Name,",
            "Speaker 4 Display Name,",
            "Speaker 4 First Name,",
            "Speaker 4 Last Name,",
            "Speaker 4 Role,",
            "Speaker 4 Title,",
            "Speaker 4 Bio,",
            "Speaker 4 Email One,",
            "Speaker 4 Email Two,",
            "Speaker 4 Organization Name,",
            "Speaker 5 Display Name,",
            "Speaker 5 First Name,",
            "Speaker 5 Last Name,",
            "Speaker 5 Role,",
            "Speaker 5 Title,",
            "Speaker 5 Bio,",
            "Speaker 5 Email One,",
            "Speaker 5 Email Two,",
            "Speaker 5 Organization Name,",
            "Speaker 6 Display Name,",
            "Speaker 6 First Name,",
            "Speaker 6 Last Name,",
            "Speaker 6 Role,",
            "Speaker 6 Title,",
            "Speaker 6 Bio,",
            "Speaker 6 Email One,",
            "Speaker 6 Email Two,",
            "Speaker 6 Organization Name,",
            "Speaker 7 Display Name,",
            "Speaker 7 First Name,",
            "Speaker 7 Last Name,",
            "Speaker 7 Role,",
            "Speaker 7 Title,",
            "Speaker 7 Bio,",
            "Speaker 7 Email One,",
            "Speaker 7 Email Two,",
            "Speaker 7 Organization Name,",
            "Speaker 8 Display Name,",
            "Speaker 8 First Name,",
            "Speaker 8 Last Name,",
            "Speaker 8 Role,",
            "Speaker 8 Title,",
            "Speaker 8 Bio,",
            "Speaker 8 Email One,",
            "Speaker 8 Email Two,",
            "Speaker 8 Organization Name,",
            "Speaker 9 Display Name,",
            "Speaker 9 First Name,",
            "Speaker 9 Last Name,",
            "Speaker 9 Role,",
            "Speaker 9 Title,",
            "Speaker 9 Bio,",
            "Speaker 9 Email One,",
            "Speaker 9 Email Two,",
            "Speaker 9 Organization Name,",
            "Speaker 10 Display Name,",
            "Speaker 10 First Name,",
            "Speaker 10 Last Name,",
            "Speaker 10 Role,",
            "Speaker 10 Title,",
            "Speaker 10 Bio,",
            "Speaker 10 Email One,",
            "Speaker 10 Email Two,",
            "Speaker 10 Organization Name,",
            "Speaker 11 Display Name,",
            "Speaker 11 First Name,",
            "Speaker 11 Last Name,",
            "Speaker 11 Role,",
            "Speaker 11 Title,",
            "Speaker 11 Bio,",
            "Speaker 11 Email One,",
            "Speaker 11 Email Two,",
            "Speaker 11 Organization Name,",
            "Speaker 12 Display Name,",
            "Speaker 12 First Name,",
            "Speaker 12 Last Name,",
            "Speaker 12 Role,",
            "Speaker 12 Title,",
            "Speaker 12 Bio,",
            "Speaker 12 Email One,",
            "Speaker 12 Email Two,",
            "Speaker 12 Organization Name,",
            "Speaker 13 Display Name,",
            "Speaker 13 First Name,",
            "Speaker 13 Last Name,",
            "Speaker 13 Role,",
            "Speaker 13 Title,",
            "Speaker 13 Bio,",
            "Speaker 13 Email One,",
            "Speaker 13 Email Two,",
            "Speaker 13 Organization Name,",
            "Speaker 14 Display Name,",
            "Speaker 14 First Name,",
            "Speaker 14 Last Name,",
            "Speaker 14 Role,",
            "Speaker 14 Title,",
            "Speaker 14 Bio,",
            "Speaker 14 Email One,",
            "Speaker 14 Email Two,",
            "Speaker 14 Organization Name,",
            "Speaker 15 Display Name,",
            "Speaker 15 First Name,",
            "Speaker 15 Last Name,",
            "Speaker 15 Role,",
            "Speaker 15 Title,",
            "Speaker 15 Bio,",
            "Speaker 15 Email One,",
            "Speaker 15 Email Two,",
            "Speaker 15 Organization Name,",
            "Speaker 16 Display Name,",
            "Speaker 16 First Name,",
            "Speaker 16 Last Name,",
            "Speaker 16 Role,",
            "Speaker 16 Title,",
            "Speaker 16 Bio,",
            "Speaker 16 Email One,",
            "Speaker 16 Email Two,",
            "Speaker 16 Organization Name,",
            "Speaker 17 Display Name,",
            "Speaker 17 First Name,",
            "Speaker 17 Last Name,",
            "Speaker 17 Role,",
            "Speaker 17 Title,",
            "Speaker 17 Bio,",
            "Speaker 17 Email One,",
            "Speaker 17 Email Two,",
            "Speaker 17 Organization Name,",
            "Speaker 18 Display Name,",
            "Speaker 18 First Name,",
            "Speaker 18 Last Name,",
            "Speaker 18 Role,",
            "Speaker 18 Title,",
            "Speaker 18 Bio,",
            "Speaker 18 Email One,",
            "Speaker 18 Email Two,",
            "Speaker 18 Organization Name,",
            "Speaker 19 Display Name,",
            "Speaker 19 First Name,",
            "Speaker 19 Last Name,",
            "Speaker 19 Role,",
            "Speaker 19 Title,",
            "Speaker 19 Bio,",
            "Speaker 19 Email One,",
            "Speaker 19 Email Two,",
            "Speaker 19 Organization Name,",
            "Speaker 20 Display Name,",
            "Speaker 20 First Name,",
            "Speaker 20 Last Name,",
            "Speaker 20 Role,",
            "Speaker 20 Title,",
            "Speaker 20 Bio,",
            "Speaker 20 Email One,",
            "Speaker 20 Email Two,",
            "Speaker 20 Organization Name,",
            "Speaker 21 Display Name,",
            "Speaker 21 First Name,",
            "Speaker 21 Last Name,",
            "Speaker 21 Role,",
            "Speaker 21 Title,",
            "Speaker 21 Bio,",
            "Speaker 21 Email One,",
            "Speaker 21 Email Two,",
            "Speaker 21 Organization Name,",
            "Speaker 22 Display Name,",
            "Speaker 22 First Name,",
            "Speaker 22 Last Name,",
            "Speaker 22 Role,",
            "Speaker 22 Title,",
            "Speaker 22 Bio,",
            "Speaker 22 Email One,",
            "Speaker 22 Email Two,",
            "Speaker 22 Organization Name,",
            "Speaker 23 Display Name,",
            "Speaker 23 First Name,",
            "Speaker 23 Last Name,",
            "Speaker 23 Role,",
            "Speaker 23 Title,",
            "Speaker 23 Bio,",
            "Speaker 23 Email One,",
            "Speaker 23 Email Two,",
            "Speaker 23 Organization Name,",
            "Speaker 24 Display Name,",
            "Speaker 24 First Name,",
            "Speaker 24 Last Name,",
            "Speaker 24 Role,",
            "Speaker 24 Title,",
            "Speaker 24 Bio,",
            "Speaker 24 Email One,",
            "Speaker 24 Email Two,",
            "Speaker 24 Organization Name,",
            "Speaker 25 Display Name,",
            "Speaker 25 First Name,",
            "Speaker 25 Last Name,",
            "Speaker 25 Role,",
            "Speaker 25 Title,",
            "Speaker 25 Bio,",
            "Speaker 25 Email One,",
            "Speaker 25 Email Two,",
            "Speaker 25 Organization Name,",
            "Delete",
        });

        public void exportAppCSV(string dest, char separator = ',')
        {
            using (StreamWriter writer = new StreamWriter(dest))
            {
                //write header
                AppCSVHeadings.ForEach(x => writer.Write(x));
                writer.Write(writer.NewLine);

                foreach (TimeSlot slot in this)
                {
                    if (slot.topicRef == null)
                    {
                        continue;
                    }
                    writer.WriteLine(slot.getAppCSVentry(separator));
                }
            }
        }

        public TimeSlotCollection getPrivateMeetings()
        {
            TimeSlotCollection result = new TimeSlotCollection();

            foreach (TimeSlot slot in this)
            {
                if (slot.topicRef == null)
                {
                    continue;
                }
                if (slot.topicRef.forPrivateMeeting == true)
                {
                    result.Add(slot);
                }
            }

            return result;
        }

        public void generateTable(StreamWriter writer, char separator = ';')
        {
            SortedDictionary<int, TimeSlotCollection> data = this.listByTrack();

            List<Sector> secotrs = this.getSectores();


            writer.Write("Sector ID{0}", separator);
            writer.Write("Sector Name{0}", separator);
            foreach (int i in data.Keys.ToList())
            {
                writer.Write(String.Format("Session {0} {1}", i, separator));
            }

            writer.Write(writer.NewLine);
            foreach (Sector sector in secotrs)
            {
                writer.Write(string.Format("{0}{1} {2}", sector.id, separator, sector.name));

                TimeSlotCollection temp = this[sector];

                foreach (int i in data.Keys.ToList())
                {
                    foreach (TimeSlot slot in temp.listByTrack(i))
                    {
                        string topic = slot.topicRef == null ? "" : slot.topicRef.id;
                        writer.Write("{0}{1}", separator, topic);
                    }
                }

                writer.Write(writer.NewLine);

            }

        }

        public void exportCSV(string dest, char separator = ';')
        {
            using (StreamWriter writer = new StreamWriter(dest))
            {
                generateTable(writer, separator);
            }
        }

        /// <summary>
        /// returns all slots listed by the days
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, TimeSlotCollection> listByDays()
        {
            Dictionary<int, TimeSlotCollection> result = new Dictionary<int, TimeSlotCollection>();

            foreach (TimeSlot slot in this)
            {
                int day = slot.range.Start.Day;

                if (result.ContainsKey(day) == false)
                {
                    result.Add(day, new TimeSlotCollection());
                }

                result[day].Add(slot);
            }

            return result;
        }

        private static Topic current;

        /// <summary>
        /// goes trough all timeslot elements but on alternate days
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static IEnumerable<TimeSlot> AlternateDays(TimeSlotCollection collection)
        {
            Dictionary<int, TimeSlotCollection> data = collection.listByDays();

            Dictionary<int, TimeSlotCollection>.KeyCollection.Enumerator dayEnum = data.Keys.GetEnumerator();

            dayEnum.MoveNext();

            bool elementsLeft;
            //bool selectLast = false;
            int i = 0;
            do
            {
                elementsLeft = false;

                if (data.Count == 0)
                {
                    break;
                }

                if (data[dayEnum.Current].Count > 0)
                {
                    elementsLeft = true;

                    IEnumerator<TimeSlot> slotEnum = data[dayEnum.Current].GetEnumerator();

                    slotEnum.MoveNext();
                    TimeSlot element = slotEnum.Current;

                    do
                    {
                        if (current.isNeighbourSession(slotEnum.Current.track) == false)
                        {
                            element = slotEnum.Current;
                            break;
                        }
                    } while (slotEnum.MoveNext());

                    data[dayEnum.Current].Remove(element);
                    yield return element;
                }

                //start from beginning if went trough already
                if (dayEnum.MoveNext() == false)
                {
                    dayEnum = data.Keys.GetEnumerator();
                    dayEnum.MoveNext();

                    i += 2;
                    //selectLast = !selectLast;
                    continue;
                }

            } while (elementsLeft);
        }

        public void assignForced(List<Topic> topics, Stream message = null)
        {
            foreach (Topic topic in topics)
            {
                if (topic.forcedSessions == null)
                {
                    continue;
                }

                //skip if topic cannot be matched or is already fully assigned
                if (topic.remainingSlots <= 0)
                {
                    continue;
                }

                TimeSlotCollection potentialSlots = this[topic.assignedSector];

                foreach (int session in topic.forcedSessions)
                {
                    if (topic.remainingSlots <= 0)
                    {
                        break;
                    }

                    TimeSlotCollection sessionSlots = potentialSlots.getBySession(session);

                    if (sessionSlots.Count <= 0)
                    {
                        Console.WriteLine("forced sessions for {0} not available in {1}",
                            topic.id, topic.assignedSector);

                        break;
                    }

                    foreach (TimeSlot slot in sessionSlots)
                    {

                        if (slot.sectorRef.canHostTopic(topic) == true)
                        {
                            if (topic.slotValid(slot, message) == true)
                            {
                                topic.assignSlot(slot);
                                slot.isForced = true;
                            }
                        }
                    }
                }
            }
        }

        public TimeSlotCollection listByTrack(int i)
        {
            TimeSlotCollection result = new TimeSlotCollection();

            foreach (TimeSlot slot in this)
            {

                if (slot.track == i)
                {
                    result.Add(slot);
                }
            }

            return result;
        }

        public SortedDictionary<int, TimeSlotCollection> listByTrack()
        {
            SortedDictionary<int, TimeSlotCollection> result = new SortedDictionary<int, TimeSlotCollection>();

            foreach (TimeSlot slot in this)
            {
                if (result.ContainsKey(slot.track) == false)
                {
                    result.Add(slot.track, new TimeSlotCollection());
                }

                result[slot.track].Add(slot);
            }

            return result;
        }

        public List<Topic> assignSlots(SortedDictionary<double, Topic> topics)
        {
            List<Topic> topicList = topics.Values.ToList();

            bool requiredLinking;
            List<Topic> ignored = new List<Topic>();

            using (MemoryStream message = new MemoryStream())
            {

                //first assign forced sessions
                this.assignForced(topics.Values.ToList(), message);

                do
                {
                    requiredLinking = false;

                    bool foundMatch = false;
                    foreach (Topic topic in topicList)
                    {
                        //hacky: thing about nicer way
                        if (topic.assignedSlots == null)
                        {
                            topic.assignedSlots = new List<TimeSlot>();
                        }

                        //skip if topic cannot be matched or is already fully assigned
                        if (topic.remainingSlots <= 0 || ignored.Contains(topic))
                        {
                            continue;
                        }

                        requiredLinking = true;

                        for (int i = 0; i < topic.remainingSlots; i++)
                        {
                            TimeSlotCollection potentialSlots = this[topic.assignedSector];

                            current = topic;

                            //iterate trough all slots are of the assigned sector
                            foreach (TimeSlot slot in AlternateDays(potentialSlots))
                            {
                                if (topic.remainingSlots <= 0)
                                {
                                    break;
                                }

                                //skip if topic already assigned to the slot
                                if (slot.topicRef == topic)
                                {
                                    continue;
                                }

                                else if (topic.slotValid(slot, message))
                                {
                                    if (slot.topicRef == null)
                                    {
                                        topic.assignSlot(slot);
                                        //generateTable(new StreamWriter(Console.OpenStandardOutput()), '\t');

                                        foundMatch = true;
                                    }
                                    else
                                    {
                                        slot.topicRef.removeSlot(slot);
                                        topic.assignSlot(slot);
                                        //generateTable(new StreamWriter(Console.OpenStandardOutput()), '\t');
                                        foundMatch = true;
                                    }
                                }
                            }
                        }

                        if (foundMatch == false)
                        {
                            if (ignored.Contains(topic) == false)
                            {
                                ignored.Add(topic);
                            }

                            Console.WriteLine(">>WARNING<<");
                            Console.WriteLine("failed to link Topic: {0} to any more time slots", topic);
                            Console.WriteLine("\tdesired sector {0}", topic.assignedSector);

                            StreamWriter writer = new StreamWriter(message);
                            writer.Flush();

                            message.Position = 0;

                            StreamReader reader = new StreamReader(message);

                            string text = reader.ReadToEnd();
                            Console.WriteLine(text);

                            Console.WriteLine();
                        }
                    }
                } while (requiredLinking == true);

                if (ConferencePlanner.currentRules.matchingRules.restrictTopicsToOneSector == false)
                {
                    return assignRemainingSlots(ignored);
                }
            }

            return ignored;
        }

        List<Topic> assignRemainingSlots(List<Topic> topics)
        {
            List<Topic> notFullyAssigned = new List<Topic>();

            foreach (Topic topic in topics)
            {
                using (MemoryStream message = new MemoryStream())
                {
                    TimeSlotCollection free = this.getUnassigned()[topic];

                    TimeSlotCollection alreadyChecked = new TimeSlotCollection();

                    foreach (TimeSlot slot in AlternateDays(free))
                    {
                        //no need to continue if everything is assigned
                        if (topic.remainingSlots <= 0)
                        {
                            break;
                        }
                        if (topic.slotValid(slot, message) == true)
                        {
                            topic.assignSlot(slot);
                            //generateTable(new StreamWriter(Console.OpenStandardOutput()), '\t');
                        }

                        alreadyChecked.Add(slot);
                    }


                    //not all required slots could be assigned; send warning
                    if (topic.remainingSlots > 0)
                    {
                        notFullyAssigned.Add(topic);

                        Console.WriteLine(">>WARNING<<");
                        Console.WriteLine("failed to link Topic: {0} to any more time slots", topic);
                        //Console.WriteLine("\tdesired sector {0}", topic.assignedSector);

                        StreamWriter writer = new StreamWriter(message);
                        writer.Flush();

                        message.Position = 0;

                        StreamReader reader = new StreamReader(message);

                        string text = reader.ReadToEnd();
                        Console.WriteLine(text);

                        Console.WriteLine();
                    }
                }
            }

            return notFullyAssigned;
        }

        public TimeSlotCollection getUnassigned()
        {
            TimeSlotCollection result = new TimeSlotCollection();

            foreach (TimeSlot slot in this)
            {
                if (slot.topicRef == null)
                {
                    result.Add(slot);
                }
            }

            return result;
        }

        public int possibleAttendees()
        {
            int attendees = 0;

            foreach (TimeSlot slot in this)
            {
                attendees += slot.sectorRef.seats;
            }

            return attendees;
        }

        /// <summary>
        /// returns all timeslots that are assigned to a sector
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TimeSlotCollection this[Sector sector]
        {
            get
            {
                TimeSlotCollection result = new TimeSlotCollection();

                foreach (TimeSlot inst in this)
                {
                    if (inst.sectorRef == sector)
                    {
                        result.Add(inst);
                    }
                }

                return result;
            }
        }

        public TimeSlotCollection getBySession(int session)
        {
            TimeSlotCollection result = new TimeSlotCollection();

            foreach (TimeSlot inst in this)
            {
                if (inst.track == session)
                {
                    result.Add(inst);
                }
            }

            return result;
        }

        public List<Sector> getSectores()
        {
            List<Sector> result = new List<Sector>();

            foreach (TimeSlot inst in this)
            {
                if (result.Contains(inst.sectorRef) == false)
                {
                    result.Add(inst.sectorRef);
                }
            }

            return result;
        }

        /// <summary>
        /// returns all timeslots that can host the topic
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TimeSlotCollection this[Topic topic]
        {
            get
            {
                TimeSlotCollection result = new TimeSlotCollection();

                foreach (TimeSlot inst in this)
                {
                    if (inst.sectorRef.canHostTopic(topic))
                    {
                        result.Add(inst);
                    }
                }

                return result;
            }
        }

        public SortedDictionary<int, DateRange> initiate(GlobalRules rules, List<Sector> sectors)
        {
            TimeSpan totalBreak = rules.breakSchedule.brakeTime;

            Console.WriteLine("total calculated break time : {0}", totalBreak);
            Console.WriteLine("session duration: {0}", rules.sessionTiming.sessionDuration);

            SortedDictionary<int, DateRange> globalSlots = new SortedDictionary<int, DateRange>();
            DateTime helper;

            int i = 1;
            foreach (DateRange range in rules.sessionTiming.days)
            {
                //for shorter code in the loop
                helper = range.Start;
                TimeSpan duration = rules.sessionTiming.sessionDuration;

                while ((helper + duration) < range.End)
                {
                    DateRange slot = new DateRange(helper, helper + duration);
                    globalSlots.Add(i++, slot);

                    foreach (DateRange breakRange in rules.breakSchedule.speakerFreeSlots)
                    {
                        if (slot.isIntersecting(breakRange) == true)
                        {
                            int day = slot.Start.Day;

                            if (ConferencePlanner.brakeTracks.ContainsKey(slot.Start.Day) == false)
                            {
                                ConferencePlanner.brakeTracks.Add(day, new List<int>());
                            }

                            int track = globalSlots.Values.ToList().IndexOf(slot) + 1;

                            if (ConferencePlanner.brakeTracks[day].Contains(track) == false)
                            {
                                ConferencePlanner.brakeTracks[day].Add(track);
                            }
                        }
                    }

                    helper += (duration + totalBreak);

                }
            }

            foreach (Sector sector in sectors)
            {
                List<DateRange> availables = globalSlots.Values.ToList();
                foreach (DateRange inst in sector.availability.selectAvailables(availables))
                {
                    TimeSlot temp = new TimeSlot()
                    {
                        range = inst,
                        sectorRef = sector,
                        track = (availables.IndexOf(inst) + 1),
                    };
                    this.Add(temp);

                    sector.possibleSessions++;
                }

                sector.remainingSessions = sector.possibleSessions;
            }

            linkSlots();

            return globalSlots;
        }

        public void linkSlots()
        {
            foreach (TimeSlotCollection collection in this.listByDays().Values)
            {
                foreach (Sector sector in collection.getSectores())
                {
                    IEnumerator<TimeSlot> slotEnum = collection[sector].GetEnumerator();
                    slotEnum.MoveNext();

                    TimeSlot current = slotEnum.Current;

                    while (slotEnum.MoveNext())
                    {
                        current.next = slotEnum.Current;
                        current = slotEnum.Current;
                    }
                }
            }
        }
    }

    public class ConferencePlanner
    {
        /// <summary>
        /// directory where all input files are found
        /// </summary>
        public static string srcDir { get; private set; }
        /// <summary>
        /// directory where the output is stored
        /// </summary>
        public string destDir { get; private set; }

        public SortedDictionary<int, DateRange> globalSlots = new SortedDictionary<int, DateRange>();

        private GlobalRules rules;

        public static GlobalRules currentRules { get; private set; }

        /// <summary>
        /// key is day, value is list of tracks
        /// </summary>
        public static Dictionary<int, List<int>> brakeTracks = new Dictionary<int, List<int>>();

        public ConferencePlanner(string srcDir, string destDir)
        {
            this.destDir = destDir;
            //TODO
            ConferencePlanner.srcDir = srcDir;
        }

        void exportSpeakerScheduleCSV(List<Speaker> speakers, StreamWriter writer, char separator = ';')
        {
            writer.Write("Speaker");
            List<int> sessions = globalSlots.Keys.ToList();

            sessions.ForEach(i=> writer.Write("{0}Session {1}",separator,i));
            writer.Write(writer.NewLine);

            foreach (Speaker speaker in speakers)
            {
                writer.Write(speaker.id);

                //to avoid null reference exception
                if (speaker.assignedSlots == null)
                {
                    speaker.assignedSlots = new List<TimeSlot>();
                }

                foreach (int i in sessions)
                {
                    writer.Write(separator);
                    List<string> topics = speaker.assignedSlots.Select(c =>
                    {
                        if (c.track == i)
                        {
                            return c.topicRef.id;
                        }

                        return "";
                    }).ToList();

                    string space = topics.Count > 1 ? " " : "";

                    topics.ForEach(str => writer.Write("{0}",str));
                }

                writer.Write(writer.NewLine);
            }
        }

        public void generateSchedule(bool deleteUnusedSpeakers = false)
        {
            //load input files and create objects
            this.rules = loadRules();
            List<Speaker> speakers = loadSpeakers();
            loadSectors();
            //loadResources();
            loadTopics();

            ConferencePlanner.currentRules = this.rules;

            List<Topic> topics = Topic.topics.Values.ToList();

            List<Session> sessions = Session.instantiateSessions(rules, topics);

            TimeSlotCollection timeSlots = new TimeSlotCollection();
            globalSlots = timeSlots.initiate(rules, Sector.sectors.Values.ToList());

            //weight sectors in order to determine which topics fit how well
            Dictionary<Topic, SortedDictionary<double, Sector>> weightedSectors = timeSlots.weightSectors(topics);

            List<Topic> ignored = Topic.matchSectors(weightedSectors);

            //list again which topics could not be matched
            if (ignored.Count > 0)
            {
                Console.WriteLine("following topics could not be fully matched to one sector:");
                foreach (Topic inst in ignored)
                {
                    Console.WriteLine("\t{0}", inst);
                    Console.WriteLine("\t\tattendees per session: {0}", inst.attendeesPerSession);
                    Console.WriteLine("\t\trequired sessions: {0}", inst.requiredSessions);

                    //topics.Remove(inst);

                    foreach (Sector sector in weightedSectors[inst].Values.ToList())
                    {
                        Console.WriteLine("\t\t\tdesired sector: {0}", sector);
                        //Console.WriteLine("\t\t\t{0}", sector);
                        Console.WriteLine("\t\t\t\tremaining sessions: {0}", sector.remainingSessions);
                        Console.WriteLine("\t\t\t\tseats in sector: {0}", sector.seats);
                    }
                    Console.WriteLine();
                }
            }

            //list which sectors with assigned topics
            foreach (Sector sector in Sector.sectors.Values.ToList())
            {

                Console.WriteLine("sector: {0}", sector);
                Console.WriteLine("\tseats: {0}", sector.seats);
                Console.WriteLine("\tpossible sessions: {0}", sector.possibleSessions);
                Console.WriteLine("\tremaining sessions: {0}", sector.remainingSessions);

                if (sector.linkedTopics != null)
                {
                    Console.WriteLine("\tassigned topics:");
                    foreach (Topic topic in sector.linkedTopics.Values.ToList())
                    {
                        Console.WriteLine("\t\tsessions{0,4} {1}", topic.requiredSessions, topic);
                    }
                }
                Console.WriteLine();
            }

            SortedDictionary<double, Topic> weightedTopics = Topic.weightTopics(topics, Speaker.speakers);

            List<Topic> ignoredTopics = timeSlots.assignSlots(weightedTopics);

            foreach (Topic inst in topics)
            {
                Console.WriteLine("\t{0}", inst);
                Console.WriteLine("\t\tattendees per session: {0}", inst.attendeesPerSession);
                Console.WriteLine("\t\trequired sessions: {0}", inst.requiredSessions);
                Console.WriteLine("\t\tmatched sessions: {0}"
                    , inst.assignedSlots == null ? 0 : inst.assignedSlots.Count);

                foreach (Sector sector in weightedSectors[inst].Values.ToList())
                {
                    Console.WriteLine("\t\t\tdesired sector: {0}", sector);
                    //Console.WriteLine("\t\t\t{0}", sector);
                    Console.WriteLine("\t\t\t\tremaining sessions: {0}", sector.remainingSessions);
                    Console.WriteLine("\t\t\t\tseats in sector: {0}", sector.seats);
                }
                Console.WriteLine();
            }

            bool notFullyAssigned = false;

            foreach (Topic topic in topics)
            {
                Console.WriteLine("Topic: {0}", topic);
                Console.WriteLine("\tattendees: {0}", topic.attendeesPerSession);
                if (topic.assignedSector != null)
                {
                    Console.WriteLine("\tpreferred sector: {0}", topic.assignedSector);
                    Console.WriteLine("\t\tseats: {0}", topic.assignedSector.seats);
                    Console.WriteLine("\t\tremaining sessions: {0}", topic.assignedSector.remainingSessions);
                }
                Console.WriteLine("\trequired sessions: {0}", topic.requiredSessions);
                Console.WriteLine("\tassigned sessions: {0}", topic.assignedSlots.Count);

                if (topic.remainingSlots > 0)
                {
                    notFullyAssigned = true;
                }

                if (topic.assignedSlots != null)
                {
                    Console.WriteLine("\tassigned slots:");
                    foreach (TimeSlot slot in topic.assignedSlots)
                    {
                        Console.WriteLine("\t{0}", slot);
                    }
                }

                Console.WriteLine();
            }

            /*foreach (Speaker speaker in speakers)
            {
                Console.WriteLine("Speaker: {0}", speaker);

                if (speaker.assignedSlots != null)
                {
                    Console.WriteLine("\tassigned slots:");
                    foreach (TimeSlot slot in speaker.assignedSlots)
                    {
                        Console.WriteLine("\t{0}", slot);
                    }
                }

                Console.WriteLine();
            }*/

            if (deleteUnusedSpeakers)
            {
                Console.WriteLine("delete unused speakers");

                string[] allFiles = Directory.GetFiles("Speakers", "*.xml");
                List<string> removed = new List<string>();
                List<Speaker> removedSpeaker = new List<Speaker>();

                foreach (Speaker speaker in speakers)
                {
                    if (speaker.assignedSlots == null || speaker.assignedSlots.Count <= 0)
                    {

                        foreach (string file in allFiles)
                        {
                            if (removed.Contains(file))
                            {
                                continue;
                            }

                            string[] lines = File.ReadAllLines(file);
                            string firstOccurrence = lines.FirstOrDefault
                                (
                                    l => l.Contains(String.Format(@"<id>{0}</id>", speaker.id))
                                );

                            if (firstOccurrence != null)
                            {
                                File.Delete(file);
                                Console.WriteLine("deleted {0}", file);
                                removed.Add(file);
                                removedSpeaker.Add(speaker);
                                break;
                            }
                        }
                    }
                }

                removedSpeaker.ForEach(e => speakers.Remove(e));
            }

            timeSlots.exportCSV(@"gsc_mapping.csv");

            timeSlots.exportAppCSV(@"appCSV.csv");

            using (StreamWriter writer = new StreamWriter(@"gsc_speaker_schedule.csv"))
            {
                exportSpeakerScheduleCSV(speakers, writer);
            }

            if (File.Exists("speakerIDs.csv"))
            {
                timeSlots.exportPrivateMeetingCSV(@"gsc_pm_mapping.csv", @"speakerIDs.csv");
            }

            if (notFullyAssigned)
            {
                Console.WriteLine("WARNING: not all topics fully assigned");

                foreach (Topic topic in ignoredTopics)
                {
                    Console.WriteLine("\t{0}", topic);
                }
            }
            else
            {
                Console.WriteLine("SUCCESS: all topics fully assigned!");
            }
        }

        public static List<T> loadObjects<T>(string dir)
        {
            List<T> objects = new List<T>();

            string path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + srcDir + @"/" + dir;
            foreach (string file in Directory.GetFiles(path, "*.xml"))
            {
                using (FileStream fs = new FileStream(file, FileMode.Open))
                {
                    DataContractSerializer ser = new DataContractSerializer(typeof(T));
                    objects.Add((T)ser.ReadObject(XmlReader.Create(fs)));
                }

                Console.WriteLine("object of type {0} loaded from {1}", typeof(T).Name, file);
            }
            return objects;
        }

        public static List<Speaker> loadSpeakers()
        {
            return loadObjects<Speaker>("Speakers");
        }
        private void loadSectors()
        {
            loadObjects<Sector>("Sectors");
        }
        public static GlobalRules loadRules()
        {
            List<GlobalRules> tempRules = loadObjects<GlobalRules>("Rules");

            GlobalRules rules;

            if (tempRules.Count == 1)
            {
                rules = tempRules[0];
            }
            else
            {
                //think about creating dedicated exception
                Console.WriteLine("{0} rules file found instead of required 1", tempRules.Count);
                Console.WriteLine("sample rules will be used");
                rules = GlobalRules.getSample();
            }

            return rules;
        }

        private void loadResources()
        {
            loadObjects<Resource>("Resources");
        }

        private void loadTopics()
        {
            loadObjects<Topic>("Topics");
        }
    }
}
