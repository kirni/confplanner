﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using backend.confPlanner;
using System.IO;

namespace backend.confPlanner
{

    /// <summary>
    /// defines how many attendees are planned for a topic
    /// </summary>
    [KnownType(typeof(Total))]
    [KnownType(typeof(PerSession))]
    [KnownType(typeof(SessionOnly))]
    [DataContract]
    public abstract class AttendeesScheduled
    {
        public abstract int requiredSessions { get; protected set; }

        public abstract int attendeesPerSession { get; protected set; }

        public abstract int totalAttendees { get;}

        /// <summary>
        /// total attendees for a topic are planned as absolute number
        /// </summary>
        [DataContract]
        public class Total : AttendeesScheduled
        {
            [DataMember]
            public int attendees;

            public override int attendeesPerSession
            {
                get
                {
                    throw new NotImplementedException();
                }

                protected set
                {
                    throw new NotImplementedException();
                }
            }

            public override int requiredSessions
            {
                get
                {
                    throw new NotImplementedException();
                }

                protected set
                {
                    throw new NotImplementedException();
                }
            }

            public override int totalAttendees
            {
                get
                {
                    return requiredSessions * attendeesPerSession;
                }            
            }           
        }
        /// <summary>
        /// total attendees for a topic are planned as attendees per session and a session number
        /// </summary>
        [DataContract]
        public class PerSession : AttendeesScheduled
        {
            [DataMember]
            int sessions;
            [DataMember]
            int attendeePerSession;            

            public override int requiredSessions
            {
                get
                {
                    return sessions;
                }

                protected set
                {
                    sessions = value;
                }
            }

            public override int attendeesPerSession
            {
                get
                {
                    return attendeePerSession;
                }

                protected set
                {
                    attendeePerSession = value;
                }
            }

            public override int totalAttendees
            {
                get
                {
                    return requiredSessions * attendeesPerSession;
                }             
            }

            public static new PerSession getSample()
            {
                return new PerSession()
                {
                    attendeePerSession = 30,
                    requiredSessions = 4,
                };
            }
        }

        [DataContract]
        public class SessionOnly : AttendeesScheduled
        {
            [DataMember]
            int sessions;

            public override int attendeesPerSession
            {
                get
                {
                    return 1;
                }

                protected set
                {
                    throw new NotImplementedException();
                }
            }          

            public override int totalAttendees
            {
                get
                {
                    return attendeesPerSession * requiredSessions;
                }
            }

            public override int requiredSessions
            {
                get
                {
                    return sessions;
                }

                protected set
                {
                    sessions = value;
                }
            }

            public SessionOnly()
            {
            }

            public SessionOnly(int sessions)
            {
                this.requiredSessions = sessions;
            }
        }

        public static AttendeesScheduled getSample()
        {
            return PerSession.getSample();
        }
    }

    [DataContract]
    public class Topic
    {
        /// <summary>
        /// defining how the attendees of this topic are scheduled
        /// </summary>
        [DataMember]
        AttendeesScheduled attendeesScheduled;

        [DataMember]
        public Availability availability { get; private set; }

        [DataMember]
        public List<int> forcedSessions { get; private set; }

        [DataMember]
        public int duration { get; private set; } 

        /// <summary>
        /// list of speakers that have to attend every session; a speaker is referred by ID
        /// </summary>
        [DataMember]
        public List<string> primarySpeakerIDs { get; private set; }

        [DataMember]
        public List<string> optionalSpeakerIDs { get; private set; }

        /// <summary>
        /// list of speakers that have to attend <see cref="maxSecondarySpeakerSessions"/> sessions; 
        /// a speaker is referred by ID
        /// </summary>
        [DataMember]
        public List<string> secondarySpeakerIDs { get; private set; }

        /// <summary>
        /// defines how many sessions a member of <see cref="secondarySpeakerIDs"/> can max attend
        /// </summary>
        [DataMember]
        public int maxSecondarySpeakerSessions { get; private set; }

        /// <summary>
        /// list of madatory resources; a resource is referred by id
        /// </summary>
        [DataMember]
        List<string> mandatoryResources;

        /// <summary>
        /// unique id ofthe topic
        /// </summary>
        [DataMember]
        public string id { get; private set; }

        /// <summary>
        /// description text of the topic
        /// </summary>
        [DataMember]
        public string description { get; private set; }      

        public bool isNeighbourSession(int session)
        {
            if (assignedSlots == null)
            {
                return false;
            }

            foreach (TimeSlot slot in assignedSlots)
            {
                if ( (slot.track + 1) == session)
                {
                    return true;
                }
                if ( (slot.track - 1) == session)
                {
                    return true;
                }
            }

            return false;
        }

        public int remainingSlots
        {
            get
            {
                if (assignedSlots == null)
                {
                    assignedSlots = new List<TimeSlot>();
                }

                int divisor = duration > 0 ? duration : 1;

                return requiredSessions - assignedSlots.Count / divisor;
            }
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        /// <summary>
        /// list of tags of the topic
        /// </summary>
        [DataMember]
        public List<string> tags { get; private set; }

        [DataMember]
        public bool forPrivateMeeting { get; private set; }

        public Sector assignedSector { get; set; }

        public bool linkedToSector { get; set; }

        public List<TimeSlot> assignedSlots = new List<TimeSlot>();

        /// <summary>
        /// checks if the slot can be used by the topic; including weight comparison for alreay assigned slots;
        /// </summary>
        /// <param name="slot"></param>
        /// <returns></returns>
        public bool slotValid(TimeSlot slot , Stream message = null, int durationOverride = -1)
        {
            StreamWriter writer = new StreamWriter(message);
            writer.WriteLine("\ttry to match {0} to session {1} in {2}",
                this.id, slot.track, slot.sectorRef);

            int tmpDuration = durationOverride == -1 ? this.duration : durationOverride;

            if (tmpDuration > 1)
            {
                if (slot.next == null)
                {
                    writer.WriteLine("\t\tnot enough slots in a row", slot, slot.topicRef);
                    writer.Flush();
                    return false;
                }

                if (slotValid(slot.next, message, tmpDuration - 1) == false)
                {
                    writer.WriteLine("\t\tduration not met", slot, slot.topicRef);
                    writer.Flush();
                    return false;
                }
            }

            if (slot.topicRef != null)
            {
                if (this.weight > slot.topicRef.weight)
                {
                    writer.WriteLine("\t\talready assigned to {1} with lower weight", slot, slot.topicRef);
                    writer.Flush();
                    return false;
                }
            }

            //ignore check of seats if the slot is for a private meeting
            if (slot.sectorRef.canHostTopic(this, this.forPrivateMeeting) == false)
            {
                writer.WriteLine("\t\t{0} can not host {1}", slot.sectorRef, this);
                writer.Flush();
                return false;
            }

            //ignore check of seats if the slot is for a private meeting
            if (slot.isForced == true)
            {
                writer.WriteLine("\t\tsession is already forced", slot, slot.sectorRef);
                writer.Flush();
                return false;
            }

            foreach (string speakerID in primarySpeakerIDs)
            {
                try
                {
                    if (Speaker.speakers[speakerID].slotValid(slot, message) == false)
                    {
                        return false;
                    }
                }
                catch (KeyNotFoundException e)
                {
                    Console.WriteLine("ERROR: speaker ID {0} not existing", speakerID);
                    throw (e);
                }
            }

            foreach (string speakerID in secondarySpeakerIDs)
            {
                try
                {
                    if (Speaker.speakers[speakerID].slotValid(slot, message) == false)
                    {
                        return false;
                    }
                }
                catch (KeyNotFoundException e)
                {
                    Console.WriteLine("ERROR: speaker ID {0} not existing", speakerID);
                    throw (e);
                }
            }

            if (availability != null)
            {
                if (availability.isAvailable(slot.range, message) == false)
                {
                    writer.WriteLine("\t\ttopic not available during session", this, slot);
                    writer.Flush();
                    return false;
                }
            }         

            writer.WriteLine("\t\tpossible");

            return true;
        }

        public void assignSlot(TimeSlot slot, int durationOverride = -1)
        {

            int tmpDuration = durationOverride == -1 ? this.duration : durationOverride;

            if (tmpDuration > 1)
            {
                assignSlot(slot.next, tmpDuration - 1);              
            }
            foreach (string speakerID in primarySpeakerIDs)
            {
                try
                {
                    Speaker.speakers[speakerID].assignSlot(slot);
                }
                catch (KeyNotFoundException e)
                {
                    Console.WriteLine("ERROR: speaker ID {0} not existing", speakerID);
                    throw (e);
                }
            }

            if (secondarySpeakerIDs != null)
            {
                foreach (string speakerID in secondarySpeakerIDs)
                {
                    try
                    {
                        Speaker.speakers[speakerID].assignSlot(slot);
                    }
                    catch (KeyNotFoundException e)
                    {
                        Console.WriteLine("ERROR: speaker ID {0} not existing", speakerID);
                        throw (e);
                    }
                }
            }

            if (optionalSpeakerIDs != null)
            {
                foreach (string speakerID in optionalSpeakerIDs)
                {
                    try
                    {
                        Speaker.speakers[speakerID].assignSlot(slot);
                    }
                    catch (KeyNotFoundException e)
                    {
                        Console.WriteLine("ERROR: speaker ID {0} not existing", speakerID);
                        throw (e);
                    }
                }
            }

            assignedSlots.Add(slot);
            slot.topicRef = this;

            Console.WriteLine("assign {0}", slot);           
        }

        public void removeSlot(TimeSlot slot)
        {
            foreach (string speakerID in primarySpeakerIDs)
            {
                try
                {
                    Speaker.speakers[speakerID].removeSlot(slot);
                }
                catch (KeyNotFoundException e)
                {
                    Console.WriteLine("ERROR: speaker ID {0} not existing", speakerID);
                    throw (e);
                }
            }

            foreach (string speakerID in secondarySpeakerIDs)
            {
                try
                {
                    Speaker.speakers[speakerID].removeSlot(slot);
                }
                catch (KeyNotFoundException e)
                {
                    Console.WriteLine("ERROR: speaker ID {0} not existing", speakerID);
                    throw (e);
                }
            }

            Console.WriteLine("remove {0}", slot);
            assignedSlots.Remove(slot);

            slot.topicRef = null;
        }


        public int requiredSessions
        {
            get
            {
                return attendeesScheduled.requiredSessions;
            }       
        }

        public int attendeesPerSession
        {
            get
            {
                if (forPrivateMeeting == true)
                {
                    return primarySpeakerIDs.Count;
                }

                return attendeesScheduled.attendeesPerSession;
            }      
        }

        public int totalAttendees
        {
            get
            {
                return requiredSessions * attendeesPerSession;
            }
        }

        public double weight { get; set; }


        /// <summary>
        /// dictionary containing all currently known speakers
        /// </summary>
        public static Dictionary<string, Topic> topics = new Dictionary<string, Topic>();

        public override string ToString()
        {
            return id;
        }

        /// <summary>
        /// attempts to match sectors to topics
        /// </summary>
        /// <param name="src"></param>
        /// <returns>list of topics that could not be matched; empty when all matched, never returns null</returns>
        public static List<Topic> matchSectors(Dictionary<Topic, SortedDictionary<double, Sector>> src)
        {

            //used exit criteria for the loop
            bool requiredLinking;

            List<Topic> ignored = new List<Topic>();

            Console.WriteLine("attempting to match topics to sectors");
            do
            {
                //is set to true when any topic still needs linking
                requiredLinking = false;
                foreach (Topic topic in src.Keys.ToList())
                {
                    //jump over topic that cannot be matched
                    if (ignored.Contains(topic))
                    {
                        continue;
                    }

                    if (topic.linkedToSector == false)
                    {
                        requiredLinking = true;

                        SortedDictionary<double, Sector> possible = src[topic];

                        if (possible.Count == 0 && topic.forPrivateMeeting == true)
                        {
                            possible = new SortedDictionary<double, Sector>();

                            List<Sector> secotrs = Sector.sectors.Values.ToList();

                            Sector best;
                            int bestDiff;

                            do
                            {
                                best = null;
                                bestDiff = int.MaxValue;

                                foreach (Sector sector in secotrs)
                                {
                                    if (sector.forPrivateMeeting == false)
                                    {
                                        continue;
                                    }

                                    int diff = topic.attendeesPerSession - sector.seats;
                                    if (bestDiff > diff)
                                    {                                      
                                        best = sector;
                                        bestDiff = diff;
                                    }
                                }

                                secotrs.Remove(best);

                                if (best.linkTopic(bestDiff, topic) == true)
                                {
                                    break;
                                }

                            } while (secotrs.Count > 0);
                        }
                        else
                        {
                            foreach (KeyValuePair<double, Sector> desired in possible)
                            {
                                if (desired.Value.linkTopic(desired.Key, topic) == true)
                                {
                                    break;
                                }
                            }
                        }

                        //failed to link topic to any secotor
                        if (topic.linkedToSector == false)
                        {
                            ignored.Add(topic);

                            Console.WriteLine(">>WARNING<<");
                            Console.WriteLine("failed to link Topic: {0} completely to any sector", topic);
                            Console.WriteLine("the topic will not be fully scheduled; consider reducing attendees for it");
                            Console.WriteLine();
                        }
                    }
                }
                //quit loop if no topic required linking
            } while (requiredLinking == true);

            return ignored;
        }

        /// <summary>
        /// adds deserialized speakers to static speakers list
        /// </summary>
        /// <param name="context"></param>
        [OnDeserialized()]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            if (this.duration == null)
            {
                this.duration = 1;
            }

            try
            {
                topics.Add(this.id, this);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("ERROR: topic key {0} is already existing", this.id);
                throw e;
            }
        }

        public static SortedDictionary<double, Topic> weightTopics(List<Topic> topics, Dictionary<string, Speaker> speakers)
        {
            List<double> weights = new List<double>();

            SortedDictionary<double, Topic> result = new SortedDictionary<double, Topic>();
            foreach (Topic topic in topics)
            {
                double weight = topic.weight;

                List<string> speakerIDs = new List<string>();

                if (topic.primarySpeakerIDs != null)
                {
                    speakerIDs.AddRange(topic.primarySpeakerIDs);
                }
                if (topic.secondarySpeakerIDs != null)
                {
                    speakerIDs.AddRange(topic.secondarySpeakerIDs);
                }
                if (topic.optionalSpeakerIDs != null)
                {
                    speakerIDs.AddRange(topic.optionalSpeakerIDs);
                }

                //weight is defined by how many people are blocked how often
                weight = weight / topic.requiredSessions / (speakerIDs.Count + 1);
              
                //hacky solution if coincidently two sectors have the same weight;
                while (weights.Contains(weight) == true)
                {
                    weight++;
                }

                weights.Add(weight);

                topic.weight = weight;
                result.Add(weight, topic);
            }

            return result;
        }

        public Topic(string id, List<string> speakers = null, List<string> speakersSecondary = null, 
            List<string> resources = null, List<string> tags = null,AttendeesScheduled attendees = null, 
            int maxSecondarySpeakerSessions = 0, string description = null, bool ignore = false,
            bool forPrivateMeeting = false, int duration = 1)
        {
            this.id = id;
            this.primarySpeakerIDs = speakers;
            this.secondarySpeakerIDs = speakersSecondary;
            this.mandatoryResources = resources;
            this.tags = tags;
            this.description = description;
            this.attendeesScheduled = attendees;
            this.maxSecondarySpeakerSessions = maxSecondarySpeakerSessions;
            this.forPrivateMeeting = forPrivateMeeting;
            this.duration = 1;

            if (ignore == false)
            {
                topics.Add(this.id, this);
            }
        }

        public static Topic getSample()
        {
            return new Topic("unique_topic_name",
                new List<string>(new string[] { "primary_speaker_id" }),
                new List<string>(new string[] { "secondary_speaker_id" }),
                new List<string>(new string[] { "resource_id" }),
                new List<string>(new string[] { "SOME", "RANDOM", "TAGS" }),
                AttendeesScheduled.getSample(),
                2,
                "nice description of the topic, including some HTML tags",
                true)
            {
                availability = Availability.getSample(),
                forPrivateMeeting = false,
                forcedSessions = new List<int>(new int[] { 1,2,}),
                optionalSpeakerIDs = new List<string>(new string[] {"optional_speaker_id", }),
                duration = 1,
            };          
        }     
    }
}
