﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using backend.confPlanner;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace backend
{
    class Program
    {
        public static string argExit
        {
            get
            {
                return "--exit";
            }
        }

        public static string argDeleteUnusedSpeaker
        {
            get
            {
                return "--delete-unused-speakers";
            }
        }

        static void Main(string[] args)
        {
            bool deleteUnusedSpeakers = false;

            if (args != null)
            {
                deleteUnusedSpeakers = args.Contains(argDeleteUnusedSpeaker);
            }

            createFolders();
            //make optional later
            createSamples();

            new ConferencePlanner("", "").generateSchedule(deleteUnusedSpeakers: deleteUnusedSpeakers);

            Console.WriteLine("done...");

            if (args != null)
            {
                if (args.Contains(argExit) == false)
                {
                    Console.WriteLine("type exit to leave");
                    while (Console.ReadLine() != "exit") ;
                }
            }
        }     

        internal static void createFolders()
        {
            Directory.CreateDirectory("Speakers");
            //Directory.CreateDirectory("Resources");
            Directory.CreateDirectory("Topics");
            Directory.CreateDirectory("Sectors");
            Directory.CreateDirectory("Rules");
            Directory.CreateDirectory("TestFiles");
            Directory.CreateDirectory("Schemas");
        }

        internal static void createSamples()
        {
            Speaker testSpeaker = Speaker.getSample();

            XmlWriterSettings settings = new XmlWriterSettings { Indent = true };

            using (XmlWriter w = XmlWriter.Create("TestFiles/testSpeaker.speaker", settings))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(Speaker));
                ser.WriteObject(w, testSpeaker);               
            }

            using (XmlWriter w = XmlWriter.Create("Schemas/SpeakerSchema.xsd", settings))
            {
                XsdDataContractExporter exporter = new XsdDataContractExporter();

                if (exporter.CanExport(typeof(Speaker)))
                {
                    exporter.Export(typeof(Speaker));
                    XmlSchemaSet mySchemas = exporter.Schemas;

                    XmlQualifiedName XmlNameValue = exporter.GetRootElementName(typeof(Speaker));
                    string EmployeeNameSpace = XmlNameValue.Namespace;

                    foreach (XmlSchema schema in mySchemas.Schemas(EmployeeNameSpace))
                    {
                        schema.Write(w);
                    }
                }
            }

            /*Resource testResource = Resource.getSample();

            using (XmlWriter w = XmlWriter.Create("TestFiles/testResource.xml", settings))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(Resource));
                ser.WriteObject(w, testResource);
            }

            using (XmlWriter w = XmlWriter.Create("Schemas/ResourceSchema.xsd", settings))
            {
                XsdDataContractExporter exporter = new XsdDataContractExporter();

                if (exporter.CanExport(typeof(Resource)))
                {
                    exporter.Export(typeof(Resource));
                    XmlSchemaSet mySchemas = exporter.Schemas;

                    XmlQualifiedName XmlNameValue = exporter.GetRootElementName(typeof(Resource));
                    string EmployeeNameSpace = XmlNameValue.Namespace;

                    foreach (XmlSchema schema in mySchemas.Schemas(EmployeeNameSpace))
                    {
                        schema.Write(w);
                    }
                }
            }*/

            Topic testTopic = Topic.getSample();

            using (XmlWriter w = XmlWriter.Create("TestFiles/testTopic.xml", settings))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(Topic));
                ser.WriteObject(w, testTopic);
            }

            using (XmlWriter w = XmlWriter.Create("Schemas/TopicSchema.xsd", settings))
            {
                XsdDataContractExporter exporter = new XsdDataContractExporter();

                if (exporter.CanExport(typeof(Topic)))
                {
                    exporter.Export(typeof(Topic));
                    XmlSchemaSet mySchemas = exporter.Schemas;

                    XmlQualifiedName XmlNameValue = exporter.GetRootElementName(typeof(Topic));
                    string EmployeeNameSpace = XmlNameValue.Namespace;

                    foreach (XmlSchema schema in mySchemas.Schemas(EmployeeNameSpace))
                    {
                        schema.Write(w);
                    }
                }
            }

            Sector testSector = Sector.getSample();

            using (XmlWriter w = XmlWriter.Create("TestFiles/testSector.xml", settings))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(Sector));
                ser.WriteObject(w, testSector);
            }

            using (XmlWriter w = XmlWriter.Create("Schemas/SectorSchema.xsd", settings))
            {
                XsdDataContractExporter exporter = new XsdDataContractExporter();

                if (exporter.CanExport(typeof(Sector)))
                {
                    exporter.Export(typeof(Sector));  
                    XmlSchemaSet mySchemas = exporter.Schemas;

                    XmlQualifiedName XmlNameValue = exporter.GetRootElementName(typeof(Sector));
                    string EmployeeNameSpace = XmlNameValue.Namespace;

                    foreach (XmlSchema schema in mySchemas.Schemas(EmployeeNameSpace))
                    {
                        schema.Write(w);
                    }
                }
            }

            GlobalRules testRules = GlobalRules.getSample();

            using (XmlWriter w = XmlWriter.Create("TestFiles/testRules.xml", settings))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(GlobalRules));
                ser.WriteObject(w, testRules);
            }

            using (XmlWriter w = XmlWriter.Create("Schemas/GlobalRulesSchema.xsd", settings))
            {
                XsdDataContractExporter exporter = new XsdDataContractExporter();

                if (exporter.CanExport(typeof(GlobalRules)))
                {
                    exporter.Export(typeof(GlobalRules));  
                    XmlSchemaSet mySchemas = exporter.Schemas;

                    XmlQualifiedName XmlNameValue = exporter.GetRootElementName(typeof(GlobalRules));
                    string EmployeeNameSpace = XmlNameValue.Namespace;

                    foreach (XmlSchema schema in mySchemas.Schemas(EmployeeNameSpace))
                    {
                        schema.Write(w);
                    }
                }
            }
        }
    }
}
