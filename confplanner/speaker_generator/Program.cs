﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using backend.confPlanner;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;

namespace speaker_generator
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Speaker> newSpeakers = new List<Speaker>();
            List<Speaker> existingSpeakers = ConferencePlanner.loadSpeakers();

            string input = "speakers.csv";

            if (args.Length == 1)
            {
                if (args[0] != "")
                {
                    input = args[0];
                }
            }

            using (StreamReader reader = new StreamReader("speakers.csv"))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    newSpeakers.Add(new Speaker(line));
                }
            }

            foreach (Speaker speaker in newSpeakers)
            {
                if (speakerIDExisting(existingSpeakers, speaker.id))
                {
                    //message shown after deserialization already
                    //Console.WriteLine("speaker ID: {0} alread existing", speaker.id);
                    continue;
                }

                XmlWriterSettings settings = new XmlWriterSettings { Indent = true };

                using (XmlWriter w = XmlWriter.Create(String.Format(@"Speakers/{0}.xml", speaker.id), settings))
                {
                    DataContractSerializer ser = new DataContractSerializer(typeof(Speaker));
                    ser.WriteObject(w, speaker);
                }

                Console.WriteLine("speaker ID: {0} created", speaker.id);
            }

            foreach (Speaker speaker in existingSpeakers)
            {
                if (speakerIDExisting(newSpeakers, speaker.id) == false)
                {
                    Console.WriteLine("Warning: speaker ID: {0} not listed in {1}", speaker.id, input);
                }
            }
        }

        static bool speakerIDExisting(List<Speaker> data, string id)
        {
            foreach (Speaker speaker in data)
            {
                if (speaker.id == id)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
